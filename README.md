# @benningfield-group/bgi2

Shared BGI Angular services.

### Publishing

1. Bump the version in `package.json` and
   `projects/benningfield-group/bgi2/package.json`
2. Make sure that the anything new is exported in
   `projects/benningfield-group/bgi2/src/public-api.ts`
2. Lint the project: `npm run lint`
3. Test the project: `npm run test`
4. Build the project: `npm run build`
5. Publish the project: `pushd dist/@benningfield-group/bgi2 && npm publish && popd`
