import { Dao } from './dao';
import { Entity } from '../entity-mapper/entity.decorator';
import { EntityMapper } from '../entity-mapper/entity-mapper';

import { Injectable } from '@angular/core';
import { Prop } from '../entity-mapper/prop.decorator';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

describe('Dao()', () => {
  @Entity
  class Note {
    @Prop
    id: number;

    @Prop
    groupId: Number;

    @Prop
    text: string;

    @Prop
    createdOn: Date;
  }

  @Injectable({
    providedIn: 'root'
  })
  class NoteDao extends Dao<Note> {
    constructor(http: HttpClient) {
      super(http);
    }

    getApiEndpoint(): string {
      return 'https://test.com/groups/:groupId/notes/:id';
    }

    getEntity(): { new(): Note } {
      return Note;
    }
  }

  let noteDao: NoteDao;
  let httpTestingController: HttpTestingController;
  let testQueryParams: HttpParams;
  let mockRecs: Note[];
  let entityMapper: EntityMapper;

  beforeEach(() => {
    entityMapper = new EntityMapper();

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        NoteDao
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    noteDao = TestBed.inject(NoteDao);

    testQueryParams = new HttpParams()
      .set('AAA', 'a')
      .set('BBB', 'b');

    mockRecs = [
      entityMapper.map({ id: 1, groupId: 1 }, Note),
      entityMapper.map({ id: 2, groupId: 1 }, Note)
    ];
  });

  it('should be created', () => {
    expect(noteDao).toBeTruthy();
  });


  describe('.get()', () => {
    it('gets a single record.', () => {

      noteDao.get(mockRecs[0])
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockRecs[0])));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/1');
      expect(req.request.method).toEqual('GET');
      req.flush(mockRecs[0]);
    });

    it('gets with queryParams.', () => {
      noteDao.get(mockRecs[0], testQueryParams)
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockRecs[0])));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/1?AAA=a&BBB=b');
      expect(req.request.method).toEqual('GET');
      req.flush(mockRecs[0]);
    });
  });


  describe('.query()', () => {
    it('queries to get an array of records.', () => {
      noteDao.query({ groupId: 1 })
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockRecs)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes');
      expect(req.request.method).toEqual('GET');
      req.flush(mockRecs);
    });

    it('queries with queryParams.', () => {
      noteDao.query({ groupId: 1 }, testQueryParams)
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockRecs)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes?AAA=a&BBB=b');
      expect(req.request.method).toEqual('GET');
      req.flush(mockRecs);
    });
  });


  describe('.save()', () => {
    let newNote: Note;

    beforeEach(() => {
      newNote = new EntityMapper().map({ groupId: 1 }, Note);
    });

    it('saves a record.', () => {
      noteDao.save(newNote)
        .subscribe(result => expect(JSON.stringify(result)).toBe(JSON.stringify(newNote)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/');
      expect(req.request.method).toEqual('POST');
      req.flush(newNote);
    });

    it('does not crash if record is null or undefined.', () => {
      // Test Null.
      const nullNote = null;

      noteDao.save(nullNote)
        .subscribe(() => 1);

      const reqNull = httpTestingController.expectOne('https://test.com/groups/notes');
      reqNull.flush({});

      // Test Undefined.
      const undefNote = undefined;

      noteDao.save(undefNote)
        .subscribe(() => 1);

      const reqUndef = httpTestingController.expectOne('https://test.com/groups/notes');
      reqUndef.flush({});
    });

    it('nulls empty string values before it posts.', () => {
      // Test an empty string.
      const newNote1 = new Note();
      newNote1.groupId = 2;
      newNote1.text = '';

      noteDao.save(newNote1)
        .subscribe(result => expect(JSON.stringify(result)).toBe(JSON.stringify(newNote1)));

      const req1 = httpTestingController.expectOne('https://test.com/groups/2/notes');
      expect(JSON.stringify(req1.request.body)).toBe('{"groupId":2,"text":null}');
      req1.flush(newNote1);

      // Test a string with a single space.
      const newNote2 = new Note();
      newNote2.groupId = 3;
      newNote2.text = ' ';

      noteDao.save(newNote2)
        .subscribe(result => expect(JSON.stringify(result)).toBe(JSON.stringify(newNote2)));

      const req2 = httpTestingController.expectOne('https://test.com/groups/3/notes');
      expect(JSON.stringify(req2.request.body)).toBe('{"groupId":3,"text":null}');
      req2.flush(newNote2);
    });

    it('updates a record.', () => {
      const updateNote = new EntityMapper().map({ id: 1, groupId: 2}, Note);

      noteDao.save(updateNote)
        .subscribe(result => expect(JSON.stringify(result)).toBe(JSON.stringify(updateNote)));

      const req = httpTestingController.expectOne('https://test.com/groups/2/notes/1');
      expect(req.request.method).toEqual('POST');
      req.flush(updateNote);
    });

    it('saves with params.', () => {
      noteDao.save(newNote, { id: 1 })
        .subscribe(result => expect(JSON.stringify(result)).toBe(JSON.stringify(newNote)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/1');
      expect(req.request.method).toEqual('POST');
      req.flush(newNote);
    });

    it('saves with queryParams.', () => {
      noteDao.save(newNote, undefined, testQueryParams)
        .subscribe(result => expect(JSON.stringify(result)).toBe(JSON.stringify(newNote)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/?AAA=a&BBB=b');
      expect(req.request.method).toEqual('POST');
      req.flush(newNote);
    });
  });


  describe('.delete()', () => {
    it('deletes a record', () => {
      noteDao.delete(mockRecs[0])
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockRecs[0])));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/1');
      expect(req.request.method).toEqual('DELETE');
      req.flush(mockRecs[0]);
    });

    it('deletes with queryParams.', () => {
      noteDao.delete(mockRecs[0], undefined, testQueryParams)
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockRecs[0])));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes/1?AAA=a&BBB=b');
      expect(req.request.method).toEqual('DELETE');
      req.flush(mockRecs[0]);
    });
  });


  describe('.put()', () => {
    let mockReplaceRecs: Note[];
    beforeEach(() => {
      mockReplaceRecs =  [
        entityMapper.map({ id: 1, groupId: 2 }, Note),
        entityMapper.map({ id: 2, groupId: 2 }, Note)
      ];
    });

    it('replaces records.', () => {
      noteDao.put(mockReplaceRecs, { groupId: 1 })
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockReplaceRecs)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes');
      expect(req.request.method).toEqual('PUT');
      req.flush(mockReplaceRecs);
    });

    it('replaces with queryParams.', () => {
      noteDao.put(mockReplaceRecs, { groupId: 1 }, testQueryParams)
        .subscribe(results =>
          expect(JSON.stringify(results)).toBe(JSON.stringify(mockReplaceRecs)));

      const req = httpTestingController.expectOne('https://test.com/groups/1/notes?AAA=a&BBB=b');
      expect(req.request.method).toEqual('PUT');
      req.flush(mockReplaceRecs);
    });
  });

});
