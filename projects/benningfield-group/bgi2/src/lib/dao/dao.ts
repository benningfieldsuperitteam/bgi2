import { HttpClient, HttpParams } from '@angular/common/http';
import { share, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { EntityMapper } from '../entity-mapper/entity-mapper';

import { UrlPathHelperService } from '../util/url-path-helper.service';

export abstract class Dao<T extends object> {
  constructor(protected http: HttpClient) {
  }

  abstract getApiEndpoint(): string;

  abstract getEntity(): { new(): T };

  /**
   * Replaces parameters in a pathname with the values from params.
   * @see [[UrlPathHelperService]]
   */
  getFormattedUrl(params: object = {}): string {
    return new UrlPathHelperService()
      .replaceRouteParams(this.getApiEndpoint(), params);
  }

  /**
   * Get one record.
   */
  get(params: object = {}, queryParams: HttpParams = new HttpParams()): Observable<T> {
    const url = this.getFormattedUrl(params);

    return this.http.get<T>(url, { params: queryParams })
      .pipe(
        map(result => new EntityMapper().map(result, this.getEntity())),
        share()
      );
  }

  /**
   * Get multiple records.
   */
  query(params: object = {}, queryParams: HttpParams = new HttpParams()): Observable<T[]> {
    const url = this.getFormattedUrl(params);

    return this.http.get<T[]>(url, { params: queryParams })
      .pipe(
        map(results => results.map(result => new EntityMapper().map(result, this.getEntity()))),
        share()
      );
  }

  /**
   * Save a record.
   */
  save(record?: T, params?: object, queryParams: HttpParams = new HttpParams()): Observable<T> {
    const url = this.getFormattedUrl(Object.assign({}, record || {}, params));

    if (record && typeof record === 'object') {
      // Convert empty string values to null in record.
      Object.keys(record)
        .forEach(key => {
          if (typeof record[key] === 'string' && record[key].trim() === '')
            record[key] = null;
        });
    }

    return this.http.post<T>(url, record, { params: queryParams })
      .pipe(
        map(result => new EntityMapper().map(result, this.getEntity())),
        share()
      );
  }

  /**
   * Delete a record.
   */
  delete(record?: T, params?: object, queryParams: HttpParams = new HttpParams()): Observable<T> {
    const url = this.getFormattedUrl(Object.assign({}, record || {}, params));

    return this.http.delete<T>(url, { params: queryParams })
      .pipe(
        map(result => new EntityMapper().map(result, this.getEntity())),
        share()
      );
  }

  /**
   * Replace records.
   */
  put(records: T[], params?: object, queryParams: HttpParams = new HttpParams()): Observable<T[]> {
    const url = this.getFormattedUrl(Object.assign({}, params ));

    return this.http.put<T[]>(url, records, { params: queryParams })
      .pipe(
        map(results => results.map(result => new EntityMapper().map(result, this.getEntity()))),
        share()
      );
  }

}
