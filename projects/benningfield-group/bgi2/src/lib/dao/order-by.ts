export class OrderBy {
  property: string;
  dir: 'ASC' | 'DESC';
}
