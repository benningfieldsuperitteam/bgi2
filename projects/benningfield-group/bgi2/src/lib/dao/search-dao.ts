import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';

import { ParameterizedCondition } from 'formn-condition-builder';

import { EntityMapper } from '../entity-mapper/entity-mapper';

import { UrlPathHelperService } from '../util/url-path-helper.service';

import { SearchResult } from './search-result';
import { SearchQuery } from './search-query';
import { OrderBy } from './order-by';

export abstract class SearchDao<T extends object> {
  constructor(protected http: HttpClient) {
    this.http = http;
  }

  abstract getApiEndpoint(): string;

  abstract getEntity(): {new(): T};

  /**
   * Replaces parameters in a pathname with the values from params.
   */
  getFormattedUrl(params: object = {}): string {
    return new UrlPathHelperService()
      .replaceRouteParams(this.getApiEndpoint(), params);
  }

  /**
   * Search for resources of type T.  They will be returned in a SearchResult
   * record under the "entities" array.
   */
  search(searchQuery: SearchQuery, routeParams?: object): Observable<SearchResult<T>> {

    // Replace any route parameters.
    const url = this.getFormattedUrl(routeParams);

    return this.http.get<SearchResult<T>>(url, {params: searchQuery.toObject()})
      .pipe(
        map(result => {
          const mapper = new EntityMapper();

          // Map result to a SearchResult instance.
          const searchResult: SearchResult<T> = mapper.map<SearchResult<T>>(result, SearchResult);

          // Map each entity in the result.
          searchResult.entities = result.entities
            .map(ent => mapper.map<T>(ent, this.getEntity()));

          return searchResult;
        }),

        share()
      );
  }
}
