import { ConditionBuilder, ParameterizedCondition, ParameterType } from 'formn-condition-builder';

import { SearchQuery } from './search-query';

describe('SearchQuery()', () => {
  describe('.where()', () => {
    it('has no condition by default.', () => {
      const query = new SearchQuery();

      expect(query.toObject().cond).not.toBeDefined();
      expect(query.toObject().params).not.toBeDefined();
    });

    it('stores the condition.', () => {
      const cb = new ConditionBuilder();
      const cond = cb.eq('firstName', ':first', 'Ben');
      const query = new SearchQuery()
        .where(cond);

      expect(query.toObject().cond).toBe(JSON.stringify({$eq: {'firstName': ':first'}}));
      expect(query.toObject().params).toBe(JSON.stringify({first: 'Ben'}));
    });

    it('stores the returned condition.', () => {
      const cb = new ConditionBuilder();
      const query = new SearchQuery()
        .where(cb => cb.eq('firstName', ':first', 'Ben'));

      expect(query.toObject().cond).toBe(JSON.stringify({$eq: {'firstName': ':first'} }));
      expect(query.toObject().params).toBe(JSON.stringify({first: 'Ben'}));
    });
  });

  describe('.limit()', () => {
    it('implies an offset of 0.', () => {
      const query = new SearchQuery()
        .limit(15);

      expect(query.toObject().offset).toBe(0);
      expect(query.toObject().rowCount).toBe(15);
    });

    it('stores the offset and rowCount.', () => {
      const query = new SearchQuery()
        .limit(100, 15);

      expect(query.toObject().offset).toBe(100);
      expect(query.toObject().rowCount).toBe(15);
    });
  });

  describe('.orderBy()', () => {
    it('has not order by default.', () => {
      const query = new SearchQuery();

      expect(query.toObject().order).not.toBeDefined();
    });

    it('implies ascending order.', () => {
      const query = new SearchQuery()
        .orderBy('firstName');

      expect(query.toObject().order).toBe(JSON.stringify([{property: 'firstName', dir: 'ASC'}]));
    });

    it('stores the order.', () => {
      const query = new SearchQuery()
        .orderBy('firstName')
        .orderBy('lastName', 'DESC');

      expect(query.toObject().order).toBe(JSON.stringify([
        {property: 'firstName', dir: 'ASC'},
        {property: 'lastName', dir: 'DESC'},
      ]));
    });
  });
});
