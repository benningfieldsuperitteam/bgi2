import { ConditionBuilder, ParameterizedCondition, ParameterType } from 'formn-condition-builder';

import { OrderBy } from './order-by';

export class SearchQuery {
  protected order: OrderBy[] = [];
  protected cond: ParameterizedCondition = null;
  protected offset = 0;
  protected rowCount = 10;

  /**
   * Set the search condition, either using a ParamterizedCondition or a
   * function that takes a ConditionBuilder instance and returns a
   * Parameterized condition.
   */
  where(whereFn: ((builder: ConditionBuilder) => ParameterizedCondition) | ParameterizedCondition): this {
    this.cond = (whereFn instanceof ParameterizedCondition) ?
      whereFn :
      whereFn(new ConditionBuilder());

    return this;
  }

  /**
   * Set the start row (offset) and number of rows to return (rowCount).  If
   * only one argument is provided then it's used as the rowCound and limit is
   * set to 0.
   */
  limit(offset: number, rowCount?: number): this {
    if (rowCount === undefined) {
      this.offset = 0;
      this.rowCount = offset;
    }
    else {
      this.offset = offset;
      this.rowCount = rowCount;
    }

    return this;
  }

  /**
   * Add an order to the orderBy array.
   */
  orderBy(property: string, dir: 'ASC' | 'DESC' = 'ASC'): this {
    this.order.push({property, dir});

    return this;
  }

  /**
   * Convert to an object for use as query parameters.
   */
  toObject(): ParameterType {
    const obj = {};

    if (this.order.length)
      obj['order'] = JSON.stringify(this.order);

    if (this.cond && Object.keys(this.cond.getCond()).length)
      obj['cond'] = JSON.stringify(this.cond.getCond());

    if (this.cond && Object.keys(this.cond.getParams()).length)
      obj['params'] = JSON.stringify(this.cond.getParams());

    obj['offset'] = this.offset;
    obj['rowCount'] = this.rowCount;

    return obj;
  }
}
