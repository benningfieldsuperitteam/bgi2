import { Entity } from '../entity-mapper/entity.decorator';
import { Prop } from '../entity-mapper/prop.decorator';

@Entity
export class SearchResult<T extends object> {
  @Prop
  count: number;

  @Prop
  offset: number;

  @Prop
  rowCount: number;
  entities: T[];
}
