import { Entity } from './entity.decorator';
import { Prop } from './prop.decorator';
import { Relationship } from './relationship.decorator';
import { PropTransformer } from './prop-transformer';
import { EntityMapper } from './entity-mapper';

describe('EntityMapper()', () => {
  // Transformer for testing. Formats a phone number.
  class PhoneFormatter implements PropTransformer {
    transform(phone: string): string {
      if (phone)
        return phone.replace(/^(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');

      return phone;
    }
  }

  // Entities for testing relationships.
  @Entity
  class Note {
    @Prop
    text: string;

    @Prop
    createdOn: Date;
  }


  // Entity class for testing mapping.
  @Entity
  class Person {
    @Prop()
    firstName: string;

    @Prop
    lastName: string;

    @Prop(new PhoneFormatter())
    phone: string;

    @Prop
    dob: Date;

    @Prop
    bowlingAvg: number;

    @Prop
    likesPizza: boolean;

    @Relationship('MANY', () => Note)
    notes: Note[];

    @Relationship('ONE', () => Note)
    note: Note;

    sayHi(): void {
      console.log(`Hi ${this.firstName}`);
    }
  }

  let mapper: EntityMapper;

  beforeEach(() => {
    mapper = new EntityMapper();
  });

  it('maps strings and booleans without transformation.', () => {
    const oPerson = {
      firstName: 'Joe',
      lastName: 'Dirt',
      likesPizza: true
    };

    const person = mapper.map(oPerson, Person);

    expect(person.firstName).toBe('Joe');
    expect(person.lastName).toBe('Dirt');
    expect(person.likesPizza).toBe(true);
    expect(person instanceof Person).toBe(true);
  });

  it('converts strings to numbers.', () => {
    const oPerson = {
      bowlingAvg: '123.4'
    };

    const person = mapper.map(oPerson, Person);

    expect(typeof person.bowlingAvg).toBe('number');
  });

  it('leaves dates alone.', () => {
    const oPerson = {
      dob: new Date(Date.UTC(2000, 0, 1, 0, 0, 0))
    };

    const person = mapper.map(oPerson, Person);

    expect(person.dob instanceof Date).toBe(true);
    expect(person.dob.toISOString()).toBe('2000-01-01T00:00:00.000Z');
  });

  it('converts ISO8601 strings.', () => {
    const oPerson = {
      dob: '2000-01-01T00:00:00.000Z'
    };

    const person = mapper.map(oPerson, Person);

    expect(person.dob instanceof Date).toBe(true);
    expect(person.dob.toISOString()).toBe('2000-01-01T00:00:00.000Z');
  });

  it('uses the supplied transformer.', () => {
    const oPerson = {
      phone: '1234567890'
    };

    const person = mapper.map(oPerson, Person);

    expect(person.phone).toBe('(123) 456-7890');
  });

  it('ignores undefined a null values.', () => {
    const oPerson = {
      firstName: undefined,
      lastName: null,
    };

    const person = mapper.map(oPerson, Person);

    expect(person.firstName).not.toBeDefined();
    expect(person.lastName).toBe(null);
  });

  it('accounts for if the relationship property is undefined.', () => {
    const oPerson = {};

    const person = mapper.map(oPerson, Person);
    expect(person.notes).not.toBeDefined();
  });

  it('accounts for if the relationship value is null.', () => {
    const oPerson = { notes: null, note: null };

    const person = mapper.map(oPerson, Person);
    expect(person.notes).toBe(null);
    expect(person.note).toBe(null);
  });

  it('recursively works for one to many relationships.', () => {
    const oPerson = {
      notes: [
        { text: 'test note 1', createdOn: '2019-10-23T18:47:06Z' },
        { text: 'test note 2', createdOn: '2019-10-21T12:00:00Z' }
      ]
    };

    const person = mapper.map(oPerson, Person);
    expect(Array.isArray(person.notes)).toBe(true);
    expect(person.notes[0] instanceof Note).toBe(true);
    expect(person.notes[0].createdOn instanceof Date).toBe(true);
    expect(person.notes[1] instanceof Note).toBe(true);
    expect(person.notes[1].createdOn instanceof Date).toBe(true);
  });

  it('recursively works for a one to one relationship.', () => {
    const oPerson = {
      note: { text: 'test note', createdOn: '2019-10-23T18:47:06Z' }
    };

    const person = mapper.map(oPerson, Person);
    expect(person.note instanceof Note).toBe(true);
    expect(person.note.createdOn instanceof Date).toBe(true);
  });

});
