import { Injectable } from '@angular/core';

import metaFactory from './metadata-factory';

/**
 * Maps raw objects to entities.
 */
@Injectable({
  providedIn: 'root'
})
export class EntityMapper {
  /**
   * Map a raw object to an entity of type T.  The entity must be
   * [[Entity]]-decorated, and only properties that are [[Prop]] decorated will
   * be mapped over.
   */
  map<T>(obj: object, Entity: {new(): T}): T {
    // Create an instance of the Entity.
    const ent = metaFactory
      .getEntityStore()
      .getEntityMetadata(Entity)
      .produceEntity();

    // This is metadata about each prop.
    const propMetas = metaFactory
      .getPropStore()
      .getPropMetadata(Entity);

    for (const propMeta of propMetas)
      ent[propMeta.name] = propMeta.transform(obj[propMeta.name]);

    // This is metadata about each relationship.
    const relationshipMetas = metaFactory
      .getRelationshipStore()
      .getRelationshipMetadata(Entity);

    for (const relMeta of relationshipMetas) {
      // If the value in the obj is null, keep the value of the entity null.
      if (obj[relMeta.name] === null) ent[relMeta.name] = null;
      // Else if the value in the obj is a truthy value, recursively call this map method.
      else if (obj[relMeta.name]) {
        // If it is one to many, we expect an array of entities.
        if (relMeta.cardinality === 'MANY') {
          ent[relMeta.name] = obj[relMeta.name].map((rel: object) => this.map(rel, relMeta.getRelationshipEntity()));
        }
        // Else, set the relationship prop to a single entity.
        else if (relMeta.cardinality === 'ONE') {
          ent[relMeta.name] = this.map(obj[relMeta.name], relMeta.getRelationshipEntity());
        }
      }
    }

    return ent;
  }
}
