import { EntityType } from './entity.type';

/**
 * Stores metadata about a single [[Entity]]-decorated class.
 */
export class EntityMetadata {
  /**
   * Init.
   * @param Entity The constructor of the [[Entity]]-decorated class.
   * @param name The name of the entity.
   */
  constructor(public Entity: EntityType, public name: string) {
  }

  /**
   * Produce an instance of Entity.
   */
  produceEntity(): any {
    return new this.Entity();
  }
}
