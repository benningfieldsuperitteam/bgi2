import { EntityType } from './entity.type';
import { EntityMetadata } from './entity-metadata';

/**
 * Storage and lookup operations for [[Entity]]-decorated classes.
 */
export class EntityStore {
  private entityMap: Map<EntityType, EntityMetadata> = new Map();

  /**
   * Add an entity.
   */
  addEntityMetadata(meta: EntityMetadata): this {
    this.entityMap.set(meta.Entity, meta);

    return this;
  }

  /**
   * Get metadata using a constructor lookup.
   */
  getEntityMetadata(Entity: EntityType): EntityMetadata {
    const meta = this.entityMap.get(Entity);

    if (meta === undefined)
      throw new Error(`Entity "${Entity.name}" not found in EntityStore.  It must be decorated with @Entity.`);

    return meta;
  }
}
