import { EntityMetadata } from './entity-metadata';
import metaFactory from './metadata-factory';

/**
 * Use this decorator to register a class for automatic type mapping.
 */
export function Entity(ctor: {new(): any}) {
  metaFactory
    .getEntityStore()
    .addEntityMetadata(new EntityMetadata(ctor, ctor.name));
}
