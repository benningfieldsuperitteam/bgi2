import { EntityStore } from './entity-store';
import { PropStore } from './prop-store';
import { RelationshipStore } from './relationship-store';

class MetadataFactory {
  private entityStore: EntityStore = new EntityStore();
  private propStore: PropStore = new PropStore();
  private relationshipStore: RelationshipStore = new RelationshipStore();

  getEntityStore(): EntityStore {
    return this.entityStore;
  }

  getPropStore(): PropStore {
    return this.propStore;
  }

  getRelationshipStore(): RelationshipStore {
    return this.relationshipStore;
  }
}

// Single instance: this is a global store for entity metadata.
export default new MetadataFactory();
