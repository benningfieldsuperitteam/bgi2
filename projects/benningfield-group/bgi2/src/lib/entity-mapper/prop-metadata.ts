import { EntityType } from './entity.type';
import { PropTransformer} from './prop-transformer';

/**
 * Holds metadata about an [[Entity]]-decorated class's [[Prop]]-decorated
 * property.
 */
export class PropMetadata {
  /**
   * Init.
   * @param Entity The constructor of the [[Entity]]-decorated class.
   * @param name The name of the decorated property.
   * @param dataType The JavaScript datatype for the property.
   * @param transformer An optional transformation function that is called when
   * mapping.  If not provided, an intelligent guess is made.
   */
  constructor(
    public Entity: EntityType,
    public name: string,
    public dataType: string,
    public transformer?: PropTransformer) {
  }

  /**
   * Transform a value using the transformer or otherwise based on dataType.
   */
  transform(val: any): any {
    if (this.transformer)
      return this.transformer.transform(val);

    if (val === null || val === undefined)
      return val;

    switch (this.dataType) {
      case 'Date':
        if (typeof val === 'string')
          return new Date(val);
        return val;

      case 'Number':
        return Number(val);

      // String | Boolean
      default:
        return val;
    }
  }
}
