import { EntityType } from './entity.type';
import { PropMetadata } from './prop-metadata';

/**
 * Storage for metadata about [[Prop]]-decorated properties in
 * [[Entity]]-decorated classes.
 */
export class PropStore {
  private entityProps: Map<EntityType, PropMetadata[]> = new Map();

  /**
   * Add metadata about a property.
   */
  addPropMetadata(meta: PropMetadata): this {
    if (!this.entityProps.has(meta.Entity))
      this.entityProps.set(meta.Entity, []);

    this.entityProps
      .get(meta.Entity)
      .push(meta);

    return this;
  }

  /**
   * Get the property metadata for an entity.
   */
  getPropMetadata(Entity: EntityType): PropMetadata[] {
    const meta = this.entityProps.get(Entity);

    if (meta === undefined)
      throw new Error(`Failed to get property metadata for type "${Entity.name}."  It must be decorated with @Entity.`);

    return meta;
  }
}
