export interface PropTransformer {
  transform(val: any): any;
}
