import 'reflect-metadata';

import { PropTransformer } from './prop-transformer';
import { PropMetadata } from './prop-metadata';
import { EntityType } from './entity.type';
import metaFactory from './metadata-factory';

/**
 * Decorate each property of an [[Entity]]-decorated class with this decorator
 * for automatic type mapping.
 */
export function Prop(target: object, propName: string): void;
export function Prop(transformer?: PropTransformer): Function;
export function Prop(targetOrTransformer: object | PropTransformer, propName?: string): void | Function {
  if (propName) {
    // Decorator not used as a producer.
    const ctor = targetOrTransformer.constructor;
    const dataType = Reflect.getMetadata('design:type', targetOrTransformer, propName);

    metaFactory
      .getPropStore()
      .addPropMetadata(new PropMetadata(ctor as EntityType, propName, dataType.name));
  }
  else {
    // Decorator used as a producer with an optional PropTransformer.
    return function(target: object, propName: string): void {
      const ctor = target.constructor;
      const dataType = Reflect.getMetadata('design:type', target, propName);
      const trans = targetOrTransformer as PropTransformer;

      metaFactory
        .getPropStore()
        .addPropMetadata(new PropMetadata(ctor as EntityType, propName, dataType.name, trans));
    };
  }
}
