import { EntityType } from './entity.type';

/**
 * Holds metadata about an [[Entity]]-decorated class's [[Relationship]]-decorated
 * propert.
 */
export class RelationshipMetadata {
  /**
   * Init.
   * @param name - The name of the decorated relationship.
   * @param Entity - The constructor of the [[Entity]]-decorated class.
   * @param getRelationshipEntity - Function to get the relationship Entity.
   * @param cardinality - The direction of the relationship. (o2o, m2o, o2m) o = one, m = many, 2 = to.
   */
  constructor(
    public name: string,
    public Entity: EntityType,
    public getRelationshipEntity: () => {new(): any},
    public cardinality: 'ONE' | 'MANY'
  ) {}


}
