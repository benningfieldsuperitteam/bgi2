import { EntityType } from './entity.type';
import { RelationshipMetadata } from './relationship-metadata';

/**
 * Storage for metadata about [[Relationship]]-decorated properties in
 * [[Entity]]-decorated classes.
 */
export class RelationshipStore {
  private relationshipMap: Map<EntityType, RelationshipMetadata[]> = new Map();

  /**
   * Add metadata about a relationship.
   */
  addRelationshipMetadata(meta: RelationshipMetadata): this {
    if (!this.relationshipMap.has(meta.Entity))
      this.relationshipMap.set(meta.Entity, []);

    this.relationshipMap
      .get(meta.Entity)
      .push(meta);

    return this;
  }

  /**
   * Get the relationship metadata for an entity.
   */
  getRelationshipMetadata(Entity: EntityType): RelationshipMetadata[] {
    // Return the array of relationships for an entity. If no relationships for the entity, return an empty array.
    return this.relationshipMap.get(Entity) ? this.relationshipMap.get(Entity) : [];
  }

}
