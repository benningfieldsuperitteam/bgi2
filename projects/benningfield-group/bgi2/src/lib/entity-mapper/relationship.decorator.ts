import { RelationshipMetadata } from './relationship-metadata';
import { EntityType } from './entity.type';
import metaFactory from './metadata-factory';

/**
 * Decorate each relationship of an [[Entity]]-decorated class with this decorator
 * for automatic type mapping of nested entities.
 */
export function Relationship(cardinality: 'ONE' | 'MANY', getRelationshipEntity: () => {new(): any}): Function {
  return function (target: object, propName: string): void {
    const ctor = target.constructor;
    metaFactory
      .getRelationshipStore()
      .addRelationshipMetadata(new RelationshipMetadata(propName, ctor as EntityType, getRelationshipEntity, cardinality));
  };
}
