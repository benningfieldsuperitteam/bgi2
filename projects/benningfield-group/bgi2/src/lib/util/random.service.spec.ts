import { TestBed } from '@angular/core/testing';

import { RandomService } from './random.service';

describe('RandomService', () => {
  let randSvc: RandomService;

  beforeEach(() => {
    TestBed.configureTestingModule({});

    randSvc = TestBed.inject(RandomService);
  });

  describe('.generateRandomString()', () => {
    it('generates a random string of length 10.', function() {
      const rand: string = randSvc.generateRandomString();

      expect(rand.length).toBe(10);
    });

    it('generates a random string of the provided length.', function() {
      const rand: string = randSvc.generateRandomString(20);

      expect(rand.length).toBe(20);
    });

    it('only contains alphanumeric characters.', function() {
      const rand: string = randSvc.generateRandomString(100);

      expect(rand.match(/^[a-z0-9]+$/)).not.toBeNull();
    });
  });
});
