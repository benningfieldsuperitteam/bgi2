import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomService {
  constructor() {
  }

  /**
   * Create a random string of length (defaults to 10).
   */
  generateRandomString(length = 10): string {
    return Array
      .from({length}, () => Math.floor(Math.random() * 36).toString(36))
      .join('');
  }
}
