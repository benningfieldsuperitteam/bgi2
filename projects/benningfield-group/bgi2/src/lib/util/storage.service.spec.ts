import { TestBed } from '@angular/core/testing';

import { StorageService } from './storage.service';

describe('StorageService', () => {
  let storageSvc: StorageService;
  let person: any;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    storageSvc = TestBed.inject(StorageService);

    // Track calls to localStorage methods.
    spyOn(Storage.prototype, 'setItem').and.callThrough();
    spyOn(Storage.prototype, 'getItem').and.callThrough();
    spyOn(Storage.prototype, 'removeItem').and.callThrough();

    // Clear local storage ('person' is used for tests) after each test.
    window.localStorage.removeItem('person');

    person = {name: 'Ben'};
  });

  describe('.setObject()', () => {
    it('stores the object in localStorage as a JSON-encoded string.', () => {
      expect(window.localStorage.getItem('person')).toBeNull();
      expect(storageSvc.setObject('person', person)).toBe(person);
      expect(Storage.prototype.setItem).toHaveBeenCalled();
      expect(window.localStorage.getItem('person')).toBe('{"name":"Ben"}');
    });
  });

  describe('.getObject()', () => {
    it('returns null if no object matching key exists in local storage.', () => {
      expect(storageSvc.getObject('person')).toBeNull();
    });

    it('returns the object if it exists in local storage.', () => {
      storageSvc.setObject('person', person);
      expect(storageSvc.getObject('person').name).toBe('Ben');
    });

    it('returns the cached object if it has already been retrieved from local storage.', () => {
      window.localStorage.setItem('person', JSON.stringify(person));
      storageSvc.getObject('person');
      storageSvc.getObject('person');
      expect((Storage.prototype.getItem as any).calls.count()).toBe(1);
    });
  });

  describe('.removeItem()', () => {
    it('removes the item from localStorage and cache.', () => {
      storageSvc.setObject('person', person);
      storageSvc.removeObject('person');
      expect(Storage.prototype.removeItem).toHaveBeenCalled();
      expect(storageSvc.getObject('person')).toBeNull();
    });
  });
});
