import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private store: object = {};

  constructor() {
  }

  /**
   * Store an object.
   * @param key The key associated with the object.
   * @param obj The object to store.
   * @return The object that was stored.
   */
  setObject(key: string, obj: any): any {
    this.store[key] = obj;
    window.localStorage.setItem(key, JSON.stringify(obj));
    return obj;
  }

  /**
   * Get the object associated with key.
   * @param key The key associated with the object.
   */
  getObject(key: string): any {
    // The object isn't in cache - pull it from local storage.
    if (this.store[key] === undefined) {
      const strVal = window.localStorage.getItem(key);

      // Item doesn't exist in local storage.
      if (strVal === null)
        return null;

      this.store[key] = JSON.parse(strVal);
    }

    return this.store[key];
  }

  /**
   * Remove the item associated with key.
   * @param key The key associated with the item to delete.
   */
  removeObject(key: string): void {
    delete this.store[key];
    window.localStorage.removeItem(key);
  }
}

