import { UrlPathHelperService } from './url-path-helper.service';

describe('UrlPathHelperService()', () => {
  let urlHelper: UrlPathHelperService;

  beforeEach(() => {
    urlHelper = new UrlPathHelperService();
  });

  describe('.replaceRouteParams()', () => {
    it('replaces the parameters.', () => {
      expect(urlHelper
        .replaceRouteParams('https://sub.domain.com/user/:userId/notes/:id', {userId: 42, id: 12}))
        .toBe('https://sub.domain.com/user/42/notes/12');
    });

    it('replaces the parameters.', () => {
      expect(urlHelper
        .replaceRouteParams('https://sub.domain.com/user/:userId/notes/:id', {userId: 42}))
        .toBe('https://sub.domain.com/user/42/notes');
    });
  });
});
