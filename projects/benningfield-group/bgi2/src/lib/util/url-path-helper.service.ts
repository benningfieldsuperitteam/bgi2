import { Injectable } from '@angular/core';

export class UrlPathHelperService {
  /**
   * Replace all route parameters in a URL.  For example, given a URL of
   * https://domain.com/api/users/:userId/notes/:id and params of
   * {userId: 42, id: 12}, return https://domain.com/api/users/42/notes/12
   */
  replaceRouteParams(url: string, params: object = {}): string {
    const regexExp = /^(https?:\/\/[^/]+)(\/[^?#]*)?/;
    const matches = regexExp.exec(url);
    const pathname =
      matches[2]
      .split('/')
      .map(pathSegment => {
        if (pathSegment[0] === ':') {
          const key = pathSegment.slice(1, pathSegment.length);
          return (key in params) ? params[key] : null;
        }
        else return pathSegment;
      })
      .filter(pathSegment => pathSegment !== null)
      .join('/');

    return `${matches[1]}${pathname}`;
  }
}
