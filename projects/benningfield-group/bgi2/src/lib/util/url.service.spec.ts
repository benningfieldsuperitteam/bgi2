import { TestBed } from '@angular/core/testing';

import { URLService } from './url.service';
import { WindowRef } from './window-ref.service';

describe('URLService', () => {
  let urlSvc: URLService;
  let mockWindow: {
    [key: string]: {
      [key: string]: any
    }
  };
  let mockWindowRef: WindowRef;

  beforeEach(() => {
    mockWindow = {
      location: {
        href: 'https://fake.com/some/path?param1=foo&param2=bar#hash1=baz&hash2=boo',
        hash: '#hash1=baz&hash2=boo',
        origin: 'https://fake.com:3210',
      }
    };

    mockWindowRef = {
      get nativeWindow(): Window {
        return mockWindow as any;
      }
    };

    TestBed.configureTestingModule({
      providers: [
        {provide: WindowRef, useValue: mockWindowRef},
        URLService
      ]
    });

    urlSvc = TestBed.inject(URLService);
  });

  describe('.getAbsURL()', () => {
    it('returns the full url using window.location.href.', () => {
      const url = urlSvc.getAbsURL();

      expect(url).toBe(mockWindow.location.href);
    });
  });

  describe('.getOrigin()', () => {
    it('returns the window.location.origin.', () => {
      const origin = urlSvc.getOrigin();

      expect(origin).toBe(mockWindow.location.origin);
    });
  });

  describe('.go()', () => {
    it('navigates to the new URL.', () => {
      const url = 'https://test.com';

      urlSvc.go(url);

      expect(url).toBe(url);
    });
  });

  describe('.getFragment()', () => {
    it('returns the fragment portion of the URL without the hash.', () => {
      expect(urlSvc.getFragment()).toBe('hash1=baz&hash2=boo');
    });
  });

  describe('.setFragment()', () => {
    it('sets the fragment.', () => {
      urlSvc.setFragment('#abc=def');
      expect(urlSvc.getFragment()).toBe('abc=def');
    });
  });

  describe('.getFragmentParams()', () => {
    it('returns an empty map when there is no hash.', () => {
      urlSvc.setFragment();

      const params = urlSvc.getFragmentParams();
      expect(params.size).toBe(0);
    });

    it('returns an empty map when the hash is not made of parameters.', () => {
      urlSvc.setFragment('asdf');

      const params = urlSvc.getFragmentParams();
      expect(params.size).toBe(0);
    });

    it('returns a map of key-value params.', () => {
      const params = urlSvc.getFragmentParams();
      expect(params.size).toBe(2);
      expect(params.get('hash1')).toBe('baz');
      expect(params.get('hash2')).toBe('boo');
    });
  });
});
