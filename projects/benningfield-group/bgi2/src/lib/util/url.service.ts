import { Injectable } from '@angular/core';

import { WindowRef } from './window-ref.service';

@Injectable({
  providedIn: 'root'
})
export class URLService {
  private window: Window;

  constructor(windowRef: WindowRef) {
    this.window = windowRef.nativeWindow;
  }

  /**
   * Get the current absolute URL.
   */
  getAbsURL(): string {
    return this.window.location.href;
  }

  /**
   * Get the origin (the protocol, hostname, and port).
   */
  getOrigin(): string {
    return this.window.location.origin;
  }

  /**
   * Go to a new URL.
   */
  go(url: string): void {
    this.window.location.href = url;
  }

  /**
   * Get the fragment portion of the URL.
   */
  getFragment(): string {
    return this.window.location.hash.substring(1);
  }

  /**
   * Set (or clear) the fragment.
   */
  setFragment(frag: string = ''): void {
    this.window.location.hash = frag;
  }

  /**
   * Get fragment parameters as a map.
   */
  getFragmentParams(): Map<string, string> {
    const frag = this.getFragment();

    if (!frag || frag.indexOf('=') === -1)
      return new Map();

    return new Map<string, string>(
      this.getFragment()
        .split('&')
        .map(keyVal => keyVal.split('=') as [string, string]));
  }
}

