import { Component, OnInit } from '@angular/core';

import { RandomService, EntityMapper } from '@benningfield-group/bgi2';

import { Person } from './person.entity';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  random = '';

  constructor(private randSvc: RandomService, private entMapper: EntityMapper) {
    const obj = {
      firstName: 'Joe',
      lastName: 'Dirt',
      phone: '1234567890',
      dob: '2019-10-09T22:47:07.584Z',
      bowlingAvg: 166.42
    };

    console.log(this.entMapper.map(obj, Person));
  }

  ngOnInit(): void {
    this.generateRandom();
  }

  generateRandom(): void {
    this.random = this.randSvc
      .generateRandomString(100);
  }
}
