import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Bgi2Module } from '@benningfield-group/bgi2';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    Bgi2Module
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
