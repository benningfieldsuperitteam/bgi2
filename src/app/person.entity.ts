import { Entity, Prop, PropTransformer } from '@benningfield-group/bgi2';

class PhoneFormatter implements PropTransformer {
  transform(phone: string): string {
    if (phone)
      return phone.replace(/^(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');

    return phone;
  }
}

@Entity
export class Person {
  @Prop()
  firstName: string;

  @Prop
  lastName: string;

  @Prop(new PhoneFormatter())
  phone: string;

  @Prop
  dob: Date;

  @Prop
  bowlingAvg: number;

  sayHi(): void {
    console.log(`Hi ${this.firstName}`);
  }
}
